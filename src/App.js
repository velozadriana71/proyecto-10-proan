import React, { useEffect, useState, useCallback } from "react";

import Tasks from "./components/Tasks/Tasks";
import NewTask from "./components/NewTask/NewTask";
import UseHttp from "./hooks/use-http";

function App() {
  const [tasks, setTasks] = useState([]);
  const transformTask = useCallback((taskObj) => {
    const loadedTasks = [];

    for (const taskKey in taskObj) {
      loadedTasks.push({ id: taskKey, text: taskObj[taskKey].text });
    }
    setTasks(loadedTasks);
  },[]);

  //     setTasks(loadedTasks);
  const {isLoading, error, sendRequest: fetchTasks}  = UseHttp(transformTask);
  
  useEffect(() => {
    fetchTasks({ url: "https://react-http-16f52-default-rtdb.firebaseio.com/task.json" });
  }, [fetchTasks]);

  const taskAddHandler = (task) => {
    setTasks((prevTasks) => prevTasks.concat(task));
  };

  return (
    <React.Fragment>
      <NewTask onAddTask={taskAddHandler} />
      <Tasks
        items={tasks}
        loading={isLoading}
        error={error}
        onFetch={fetchTasks}
      />
    </React.Fragment>
  );
}

export default App;
